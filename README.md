# React Embed with JWT Token & Site based Authentication

This sample repo works with JWT token issued by OIDC provider.
Additional check has been added for site based authorization.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install all dependancies.
This sample app has been tested with Python 3.8 & [conda](https://docs.anaconda.com/anaconda/install/).

```bash
pip install requirement.txt
```

## Limitations 

It currently works with only 1 site `9f139ef9-12bf-474a-9889-f486a4c9c7f1` - Aaron's site (my site, not your site).

This is a just a test - ignore.